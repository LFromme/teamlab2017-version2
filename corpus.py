from fextractor import FeatureExtractor
from reader import Reader
import pickle

class Corpus:

    def __init__(self, train_path, test_path, dev_path):
        self.train_path = train_path
        self.test_path = test_path
        self.dev_path = dev_path
        self.training_data = None
        self.testing_data = None
        self.dev_data = None
        self.gold = None
        self.features = None
        self.ftable = None

    def read_training(self):
        r = Reader(self.train_path)
        self.training_data = r.read_in()

    def read_testing(self):
        r = Reader(self.test_path)
        self.testing_data = r.read_in()

    def read_dev(self):
        r = Reader(self.dev_path)
        self.dev_data = r.read_in()

    def construct_corpus(self, load_model = False):
        self.read_dev()
        self.read_training()
        self.read_testing()
        
        if load_model:
            with open("feature_list_full.p", "rb") as f:
                print "loading feature list from file..."
                self.features = pickle.load(f)
                f = FeatureExtractor(self.training_data)
                f.make_doc_features()
        else:
            f = FeatureExtractor(self.training_data)
            self.features = f.extract_features()
        f = FeatureExtractor(self.testing_data)
        f.make_doc_features()
        #print len(self.features)
        self.ftable = {ftr:idx for idx, ftr in enumerate(self.features)}
        #print self.ftable.items()
        for document in self.training_data:
            document.construct_vector(self.features, self.ftable)
        for document in self.testing_data:
            document.construct_vector(self.features, self.ftable)
            #print document.doc_features
        for document in self.dev_data:
            document.construct_vector(self.features, self.ftable)

    def save_features(self, path):
        with open(path, "wb") as f:
            pickle.dump(self.features, f)
        print "saved feature list to " + path




