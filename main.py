from sent_analyser import Sentiment_analyser
from corpus import Corpus

if __name__ == '__main__':
    c = Corpus('/mount/projekte7/tcl/klingern/teaching/teamlab-ss2017/emotion/train.csv','/mount/projekte7/tcl/klingern/teaching/teamlab-ss2017/emotion/dev.csv','twitter_train_100.csv')
    #c = Corpus('twitter_train_10000.csv','/mount/projekte7/tcl/klingern/teaching/teamlab-ss2017/emotion/dev.csv','twitter_train_100.csv')
    c.construct_corpus()
    #c.save_features("features_full.p")
    s = Sentiment_analyser(c)
    s.training(20)
    s.testing("model_full.p")
    s.evaluation()
    #s.testing()
    #s.evaluation()

