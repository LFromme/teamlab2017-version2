from reader import Reader 
from perceptron import Perceptron
from corpus import Corpus
from evaluator import Evaluator

class Sentiment_analyser:

    def __init__(self, corpus):
        self.classes = sorted(set([d.sentiment for d in corpus.training_data]))
        self.perceptron = None
        self.corpus = corpus
        self.predicted = None

    def training(self, epochs):
        self.perceptron = Perceptron(self.corpus.features, self.classes, self.corpus.training_data)
        self.perceptron.training(epochs)

    def testing(self, model_path, dev = False):
        self.perceptron = Perceptron(self.corpus.features, self.classes, self.corpus.testing_data, model_path)
        if dev == True:
            #print len(self.corpus.dev_data)
            self.predicted = self.perceptron.predict_corpus(self.corpus.dev_data)
        else: 
            self.predicted = self.perceptron.predict_corpus(self.corpus.testing_data)
        
    def evaluation(self):
        print(self.corpus.test_path, self.predicted)
        e = Evaluator(self.corpus.test_path, self.predicted)
        if self.predicted != None and self.corpus != None:
            e.eval()
            e.display()
        else:
            raise ValueError("Corpus or prediction doesn't seem to be constructed yet")



