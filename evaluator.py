from __future__ import division

import sys
from reader import Reader
from document import Document
from collections import Counter

class Evaluator:
    """
    reads in gold and predicted files and calculates recal / precision scores for these files
    """
    def __init__(self, gold, predicted):
        r = Reader(gold)
        #s = Reader(predicted)

        self.gold_docs = r.read_in()
        self.predicted_docs = predicted
        self.macro_precision = 0
        self.macro_recall = 0
        self.micro_precision = 0
        self.micro_recall = 0
        self.sentiment_dict = {}
        self.scores = {}

        if len(self.gold_docs) != len(self.predicted_docs):
            raise ValueError("List of gold and predicted documents are of different length!")
            sys.exit(-1)

    """
    evaluates on the gold and predicted file specified in the constructor
    """
    def eval(self):
        sentiments_gold = [d.sentiment for d in self.gold_docs]
        #sentiments_predicted = [d.sentiment for d in self.predicted_docs]
        sentiments_predicted = self.predicted_docs
        allsent = set(sentiments_predicted)
        eval_d = {}
        for sent in allsent:
            eval_list = map(lambda x: self.evaluate(sent,x), zip(sentiments_gold,sentiments_predicted))
            eval_d[sent] = list(eval_list)

        #make a dictionary of precision and recall for each sentiment
        for key in eval_d:
            self.scores[key] = self.precision_recall(eval_d[key])
        
        #calculate macro precision and macro recall over all sentiments
        self.macro_precision, self.macro_recall = self.macro_average([self.precision_recall(eval_d[key]) for key in eval_d])

        
        aggr_tp = sum([self.scores[key][2] for key in self.scores])
        aggr_tn = sum([self.scores[key][3] for key in self.scores])
        aggr_fn = sum([self.scores[key][4] for key in self.scores])
        aggr_fp = sum([self.scores[key][5] for key in self.scores])

        self.micro_precision = aggr_tp/(aggr_tp+aggr_fp)
        self.micro_recall = aggr_tp/(aggr_tp+aggr_fn)
        #self.sentiment_dict = eval_d
        
    def eval_param(self):
        sentiments_gold = [d.sentiment for d in self.gold_docs]
        
        sentiments_predicted = []
        for d in self.gold_docs:
            try:
                sentiments_predicted.append(d.predicted_sentiment)
            except:
                print "no predicted sentiment found for document, assuming happy"
                sentiments_predicted.append("happy")
        allsent = set(sentiments_predicted)
        eval_d = {}
        for sent in allsent:
            eval_list = map(lambda x: self.evaluate(sent,x), zip(sentiments_gold,sentiments_predicted))
            eval_d[sent] = list(eval_list)

        #make a dictionary of precision and recall for each sentiment
        for key in eval_d:
            self.scores[key] = self.precision_recall(eval_d[key])
        
        #calculate macro precision and macro recall over all sentiments
        self.macro_precision, self.macro_recall = self.macro_average([self.precision_recall(eval_d[key]) for key in eval_d])

        
        aggr_tp = sum([self.scores[key][2] for key in self.scores])
        aggr_tn = sum([self.scores[key][3] for key in self.scores])
        aggr_fn = sum([self.scores[key][4] for key in self.scores])
        aggr_fp = sum([self.scores[key][5] for key in self.scores])


        self.micro_precision = aggr_tp/(aggr_tp+aggr_fp)
        self.micro_recall = aggr_tp/(aggr_tp+aggr_fn)
        self.display()

    """
    maps matches/nonmatches to Error Types. Internal method, do not call.
    """
    def evaluate(self,sent,zipped):
        gold = zipped[0]
        predicted = zipped[1]
        if predicted != sent: 
            if gold != sent:
                return 'TN'
            else:
                return 'FN'
        else:
            if gold != sent:
                return 'FP'
            else:
                return 'TP'

    """
    calculates precision and recall given the counts of Error Types. Internal method, do not call.
    """
    def precision_recall(self,eval_list):
        tp = eval_list.count('TP')
        tn = eval_list.count('TN')
        fn = eval_list.count('FN')
        fp = eval_list.count('FP')
        try:
            precision = tp/(tp+fp)
            recall = tp/(tp+fn)
        except:
            print "ERROR: divisor of precision or recall 0, setting both to 0"
            precision = 0
            recall = 0
        return (precision, recall, tp, tn, fn, fp)

    """
    calculates macro average from precision and recall. Internal method, do not call.
    """
    def macro_average(self,l):
        macro_precision = self.average([element[0] for element in l])
        macro_recall = self.average([element[1] for element in l])
        return (macro_precision,macro_recall)

    def average(self,l):
        #print l
        return sum(l)/len(l)

    """
    displays all scores in human readable format
    """

    def display(self): 
        for key in self.scores:
            print(key.upper())
            print('precision: %.4f ' % (self.scores[key][0]))
            print('recall: %.4f ' % (self.scores[key][1]))
            print('')
        
        print("MACRO_PRECISION: %.4f" % (self.macro_precision))
        print("MACRO_RECALL: %.4f" % (self.macro_recall))
        print('')
        print("MICRO_PRECISION: %.4f" % (self.micro_precision))
        print("MICRO_RECALL: %.4f" % (self.micro_recall))




