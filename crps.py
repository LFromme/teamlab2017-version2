from fextractor import FeatureExtractor
from reader import Reader

class Corpus:

    def __init__(self, train_path, test_path, dev_path):
        self.train_path = train_path
        self.test_path = test_path
        self.dev_path = dev_path
        self.training_data = read_training(self.train_path)
        self.testing_data = read_testing(self.testing_data, False)
        self.dev_data = read_testing(self.dev_data)
        self.gold = None
        self.features = construct_corpus()

    def read(self, path, training = True):
        r = Reader(path)
        data = r.read_in(training)
        return data

    def construct_corpus(self):
        f = FeatureExtractor(self.training_data)
        self.features = f.extract_features()
        for document in self.training_data:
            document.construct_vector(self.features)
        for document in self.testing_data:
            document.construct_vector(self.features)
        for document in self.dev_data:
            document.construct_vector(self.features)
        return features
