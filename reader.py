from document import Document
import os
import io

class Reader:
    """
    creates lists of documents from files
    """
    def __init__(self, path):
        self.path = path

        """
        if not self.check_validity(path):
            error_message = 'File to be read at ' + path + ' not found'
            raise IOError(error_message)    
        """        

    """
    creates documents from the read in file and returns all documents stored in a list 
    """
    def read_in(self, train_file=True):
        Dlist = []
        with io.open(self.path, 'r', encoding='utf-8') as tweetfile:
            if os.path.isfile(self.path):
                while True:
                    tweetline = tweetfile.readline().split('\t')
                    if not tweetline[0]:
                        break
                    if train_file:
                        D = Document(tweetline[8].strip('\n'), tweetline[0].strip('\n'))
                    else:
                        D = Document("", tweetline[0].strip('\n'))
                    Dlist.append(D)
        return Dlist
