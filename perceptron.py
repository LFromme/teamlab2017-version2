from __future__ import division
from document import Document
import numpy as np
import pickle

class Perceptron:
    """
    predicts the class for a given instance based on its feature vector and a model
    """
    def __init__(self, features, classes, data, model_path = ""):
        self.weights = np.random.rand(len(classes),len(features))
        self.features = features
        self.data = data
        self.data_length = len(data)
        self.classes = classes
        self.legend = {key:value for (key,value) in enumerate(classes)}
        if model_path:
            with open(model_path, "rb") as f:
                print "loading model..."
                model = pickle.load(f)
                self.weights = model[1]

    def training(self, epochs):
        print "begin training"
        for i in range(epochs):
            correct = 0
            false = 0
            i = 0
            for d in self.data:
                x_vec = d.feature_vector
                gold = d.sentiment
                pred = self.test_predict(d)
                #print pred
                for index,c in enumerate(self.classes):
                    if pred != c and c == gold:
                        for f in x_vec:
                            self.weights[index][f] += 0.5
                        #self.weights[index] = np.add(x_vec,self.weights[index])
                    elif pred == c and c != gold:
                        for f in x_vec:
                            self.weights[index][f] -= 0.5
                        #self.weights[index] = np.subtract(x_vec,self.weights[index])
                #print pred
                if pred == gold:
                    correct += 1
                else:
                    false += 1

                i += 1

                if i % 10000 == 0:
                    accuracy = correct / (correct + false) * 100
                    progress = i / self.data_length * 100
                    print "predicted %.2f percent of documents at %.2f percent accuracy" % (progress, accuracy)
            self.save("model_full.p")
                    
    def test_predict(self,tweet):
        classwise_scores = []

        x_vec = tweet.feature_vector
        for index,y_vec in enumerate(self.weights):
            classwise_scores.append((self.legend[index],self.sparse_scalar(x_vec,y_vec)))
        #TODO max function is not documented on ties, use something else, like sort and max. Python 3 max function works.
        #print classwise_scores
        predicted = max(classwise_scores, key = lambda x : x[1])
        return predicted[0]

    def predict_corpus(self,test_data):
        return [self.test_predict(d) for d in test_data]
        #for d in test_data:
        #    d.predicted_sentiment = self.test_predict(d)

    def sparse_scalar(self, x_vec, y_vec):
        score = 0
        for f in x_vec:
            score += y_vec[f]
        return score

    def save(self, path="model_small.p"):
        with open(path, "wb") as f:
            pickle.dump((self.features, self.weights), f)
        print "saved successfully to " + path

