class Document:

    def __init__(self, content, sentiment):

        self.sentiment = sentiment
        self.content = content
        self.feature_vector = []
        self.doc_features = []
        self.predicted_sentiment = ""

    def construct_vector(self, features, ftable):
        #print self.doc_features
        for f in self.doc_features:
            try:
                self.feature_vector.append(ftable[f])
            except:
                print "WARNING: feature of testing document was not found in the training features"
 