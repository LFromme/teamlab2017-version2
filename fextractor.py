from __future__ import division
import re
from document import Document
from reader import Reader
from collections import Counter
import pickle

class FeatureExtractor:
    
    def __init__(self, Documents):
        self.documents = Documents
        self.content = []
        self.features = []
        self.docslen = len(Documents)
        for d in Documents:
            self.content.append(d.content)

    def make_doc_features(self):
        i = 0
        for s in self.documents:
            tags = re.findall(r"#(\w+)", s.content)
            for t in tags:
                form = t.lower()
                s.doc_features.append(form)

            words = set(s.content.split())
            for w in words:
                s.doc_features.append(w)


            if "!" in s.content:
                s.doc_features.append('excl')

            if i % 1000 == 0:
                print i /self.docslen*100,
                print "percent of doc-features inserted"
            i += 1

    def extract_features(self):
        i = 0
        for s in self.documents:
            #HASHTAG-Feature
            tags = re.findall(r"#(\w+)", s.content)
            for t in tags:
                form = t.lower()
                s.doc_features.append(form)
                self.features.append(form)

            #Exclamation Mark Feature
            if "!" in s.content:
                self.features.append('excl')
                s.doc_features.append('excl')


            words = set(s.content.split())
            for w in words:
                s.doc_features.append(w)
                self.features.append(w)
                


            if i % 1000 == 0:
                print i /self.docslen*100,
                print "percent of features processed"
            i += 1
            """
            #Emote-Feature
            emotes = re.findall(r'^[a-zA-Z0-9]{2,})'
            for e in emotes:
                self.features.append('em ' + e)
                s.doc_features.append('em ' + e)
            """
        self.features = sorted(set(self.features))
        return self.features
        #pickle.dump(self.features, "feature_list.p")

    def save(self, path = "features.p"):
        with open(path, "wb") as f:
            pickle.dump(self.features, f)
        #print features_saved
            
